repositories {
	mavenCentral()
}

plugins {
	kotlin("multiplatform") version "1.7.30"
	java
}

kotlin {
	js {
		browser()
	}
	jvm {
		compilations.all {
			kotlinOptions.jvmTarget = "18"
		}
	}

	sourceSets {
		val commonMain by getting {
			dependencies {
				kotlin("multiplatform")
			}
		}

		val jvmTest by getting {
			dependencies {
				implementation("io.kotest:kotest-runner-junit5-jvm:5.4.2")
				implementation("io.kotest:kotest-assertions-core:5.4.2")
			}
		}
	}
}

dependencies {
	val testImplementation by configurations
}


tasks.withType<Test>().configureEach {
	useJUnitPlatform()
}
