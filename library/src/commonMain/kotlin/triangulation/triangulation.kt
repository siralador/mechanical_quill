package dev.alador.mechanical_quill.triangulation

import dev.alador.mechanical_quill.*
import kotlin.math.absoluteValue

data class Vertex(
	val pos: Point<F64Unit>,
	val style: Style<F64Unit>,
) {}

private data class Node(
	val point: Point<F64Unit>,
	val edges: MutableMap<Edge, Node>,
	val triangles: MutableList<Triangle>,
) {}

fun triangulate(polygons: List<List<Point<F64Unit>>>, do_even_odd: Boolean = true): List<Triangle> {
	val my_polygons = polygons.filter { it.size >= 3 }
	if (my_polygons.isEmpty()) return listOf();

	//Create all edges
	val edges = HashSet<Edge>()
	var min_x = my_polygons[0][0].x.v
	var max_x = my_polygons[0][0].x.v
	var min_y = my_polygons[0][0].y.v
	var max_y = my_polygons[0][0].y.v
	for (polygon in my_polygons) {
		for (i in 0..(polygon.size - 2)) {
			edges.add(Edge(polygon[i], polygon[i+1]))
			val point = polygon[i+1]
			if (point.x.v < min_x) min_x = point.x.v
			else if (point.x.v > max_x) max_x = point.x.v
			if (point.y.v < min_y) min_y = point.y.v
			else if (point.y.v > max_y) max_y = point.y.v
		}
		//Connect the ends of the polygon if they aren't already
		val last_edge = Edge(polygon[0], polygon[polygon.size-1])
		if (last_edge.a != last_edge.b) edges.add(last_edge)
	}
	
	//Intersection detection and breaking
	val hard_edges = edges.toMutableList()
	var i: Int = 0
	while (i < hard_edges.size) {
		var o: Int = i + 1 //Scan forward in the list, everything behind is already accounted for
		while (o < hard_edges.size) {
			if (i == o) {
				o += 1
				continue
			}
			val a = hard_edges[i]
			val b = hard_edges[o]

			//See if they intersect and need to be replaced
			a.cut_with(b)?.let { new_edges ->
				//Remove both edge a and edge b from the list
				hard_edges.removeAt(i)
				o -= 1
				hard_edges.removeAt(o)
				//Add in all of the new edges
				for (edge in new_edges) {
					hard_edges.add(i, edge)
					o += 1
				}
			}
			o += 1
		}
		i = i + 1
	}

	//Build a mesh of all of the points
	val mesh = mutableMapOf<Point<F64Unit>, Node>()
	for (edge in hard_edges) {
		val node_a = mesh.getOrPut(edge.a) { Node(edge.a, mutableMapOf(), mutableListOf()) }
		val node_b = mesh.getOrPut(edge.b) { Node(edge.b, mutableMapOf(), mutableListOf()) }
		node_a.edges.put(edge, node_b)
		node_b.edges.put(edge, node_a)
	}

	//For each point in the mesh, step to all combinations of two neighbors, and try to form them into a triangle
	val soft_edges = mutableListOf<Edge>()
	val triangles = mutableListOf<Triangle>()
	for (node in mesh.values) {
		for (a in node.edges.values) {
			for (b in node.edges.values) {
				if (a == b) continue	

				//Are they already connected?
				val c_edge = a.edges.asSequence().firstOrNull { (_, node) -> node === b }?.let { (edge, _) -> edge}
				val triangle = 
					if (c_edge != null) Triangle(node.point, a.point, b.point)
					//Can an edge be created to connect the two?
					else {
						val new_edge = Edge(a.point, b.point)
						//Ensure the edge does not exist and does not intersect with any other edges
						val is_valid = soft_edges.find { new_edge == it || it.should_cut_at(new_edge) != null } == null
							&& hard_edges.find { it.should_cut_at(new_edge) != null } == null
						println("new_edge $new_edge $is_valid")
						if (is_valid) {
							soft_edges.add(new_edge)
							a.edges.put(new_edge, b)
							b.edges.put(new_edge, a)
							Triangle(node.point, a.point, b.point)
						}
						else continue
					}

				//Ensure the triangle doesn't already exist
				if (node.triangles.find { it == triangle } == null) {
					//Woo! new triangle
					node.triangles.add(triangle)
					a.triangles.add(triangle)
					b.triangles.add(triangle)
					triangles.add(triangle)
				}
			}
		}
	}

	if (do_even_odd) {
		//Return only the triangles that pass the even odd rule
		val extension = (max_x - min_x)
			.let {
				val other = (max_y - min_y)
				if (other > it) other else it
			}
			.let { it * it }
		return triangles.filter {
			val centroid = it.centroid()
			val target_edge = Edge(it.a, it.b)
			println("=====")
			println(centroid)
			println(target_edge)
			println(Edge(centroid, target_edge.midpoint()))
			val test_edge = Edge(centroid, target_edge.midpoint())
				.extend(extension)
			println(test_edge)
			val edge_passes = hard_edges.asSequence()
				.filter {
					println("${it} -> ${test_edge.should_cut_at(it)}")
					test_edge.should_cut_at(it) != null }
				.count()
			println(hard_edges.size)
			println(edge_passes)
			edge_passes % 2 == 1
		}
	}
	else return triangles
}
