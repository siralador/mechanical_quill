package dev.alador.mechanical_quill.triangulation

import dev.alador.mechanical_quill.*
import kotlin.math.sqrt

data class PolygonizationParameters(
	val max_stepper_segment_length: Double = 0.1,
	val min_stepper_segment_length: Double = 0.01,
	val max_stepper_time: Double = 0.01,
	val derivative_step: Double = 0.000000001,
) {}
data class Polygonization(
	val polygons: List<List<Point<F64Unit>>>,
	val style: Style<F64Unit>,
) {}

fun polygonize(
	command: Command<F64Unit>,
	params: PolygonizationParameters
): List<Polygonization> {
	if (command is EndOfDocumentCommand) return listOf()
	if (command is FillPolygonCommand) return listOf(Polygonization(listOf(command.points), command.style))
	if (command is FillRectanglesCommand) return command.rectangles.map {
		Polygonization(
			listOf(listOf(
				it.origin,
				Point(it.origin.x + it.width, it.origin.y),
				Point(it.origin.x + it.width, it.origin.y + it.height),
				Point(it.origin.x, it.origin.y + it.height),
			)),
			command.style
		)
	}
	/*if (command is FillPathCommand) return command.path.segments.map {
				
	}*/
	throw Exception("Unknown command")
}

fun polygonize_action(
	segment: MutableList<Point<F64Unit>>,
	action: PathAction<F64Unit>,
	params: PolygonizationParameters,
) {
	if (segment.isEmpty()) throw RuntimeException("Polygonize Action called on empty segment");	
	if (action is LineAction) segment.add(action.end);
	else if (action is HorizontalAction) segment.add(Point(action.end_x, segment.last().y))
	else if (action is VerticalAction) segment.add(Point(segment.last().x, action.end_y))
	else if (action is CubicBezierAction) cubic_bezier_stepper(segment, action.control_0, action.control_1, action.end)
	else if (action is ArcCircleAction) point_stepper(
		
		segment
	)
	throw Exception("Unknown path action")
}

private fun Double.square() = this * this
private fun F64Unit.square() = this * this

fun point_stepper(
	func: (time: Double) -> Point<F64Unit>, 
	segment: MutableList<Point<F64Unit>>
	params: PolygonizationParameters,
) {
	var time = 0.0
	while (time < 1.0) {
		val point = segment.last()
		val derivative_point = func(time + params.derivative_step)
		val arc_length_derivative = sqrt(
			((derivative_point.x - point.x) / params.derivative_step).square().v
			+ ((derivative_point.y - point.y) / params.derivative_step).square().v
		)
		var guess_offset = params.max_stepper_segment_length / arc_length_derivative //Units divided by Units/Time
		if (guess_offset > params.max_stepper_time) guess_offset = params.max_stepper_time		

		var guess_time = time + guess_offset
		var guessed_point = func(guess_time)
		var length = point.distance_to(guessed_point)
		while (length < params.min_stepper_segment_length || length > params.max_stepper_segment_length) {
			if (length < params.min_stepper_segment_length) {
				guess_offset *= 1.5
			} else if (length > params.max_stepper_segment_length) {
				guess_offset *= 0.75
			}

			guess_time = time + guess_offset
			guessed_point = func(guess_time)
			length = point.distance_to(guessed_point)
		}
		segment.add(guessed_point)
		time = guess_time
	}
	time = 1.0
	segment[segment.size - 1] = func(time)
}

private fun bezier_point(lines: List<Edge>, time: Double): Point<F64Unit>  {
	var segments = lines
	while (segments.size > 1) {
		var new_segments = mutableListOf<Edge>()
		for (i in 1..(segments.size-1)) {
			new_segments.add(Edge(
				segments[i-1].percent_point(time),
				segments[i].percent_point(time)
			))
		}
	}
	return segments[0].percent_point(time)
}

private fun cubic_bezier_stepper(
	segment: MutableList<Point<F64Unit>>,
	control_0: Point<F64Unit>,
	control_1: Point<F64Unit>,
	end: Point<F64Unit>,
	params: PolygonizationParameters,
) {
	val start = segment.last()
	point_stepper(
		{ bezier_point(listOf(  Edge(start, control_0),  Edge(control_1, end)  ), it) },
		segment,
		params,
	)
}

private fun ellipse_points(
	segment: MutableList<Point<F64Unit>>,
	rx: Double,
	ry: Double,
	rotation: Double, 
	large_arc: Boolean,
	sweep: Boolean,
	end: Point<F64Unit>
) {
	val start = segment.last();
	val chord_angle = Edge(start, end).atan2()
	//Operate on a non-rotated ellipse
	val adjusted_chord_angle = chord_angle - rotation
	//Squash the ellipse into a unit circle
}
