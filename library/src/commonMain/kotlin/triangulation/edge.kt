package dev.alador.mechanical_quill.triangulation

import kotlin.math.absoluteValue
import kotlin.math.atan2
import kotlin.math.PI
import kotlin.math.tan
import kotlin.math.sqrt

import dev.alador.mechanical_quill.*

fun F64Unit.is_yesnt() = isNaN() || isInfinite()

private class OrderedEdge(val a: Point<F64Unit>, val b: Point<F64Unit>) {}
data class Edge(val a: Point<F64Unit>, val b: Point<F64Unit>) {
	override operator fun equals(other: Any?): Boolean {
		if (other is Edge) return (a == other.a && b == other.b) || (a == other.b && b == other.a)
		return false
	}
	override fun hashCode() =
		if (a.x < b.x || (a.x == b.x && a.y < b.y)) OrderedEdge(a, b).hashCode()
		else OrderedEdge(b, a).hashCode()

	/** Returns the slope of the line segment */
	fun slope() = ((b.y - a.y) / (b.x - a.x)).let { if (it.is_yesnt()) F64Unit(Double.NaN) else it }
	/** Returns the tangent slope of the line segment, going counter-clockwise */
	fun tangent_slope() = slope().let { when (it.v) {
		Double.NaN -> F64Unit(0.0)
		0.0 -> F64Unit(Double.NaN)
		else -> F64Unit(-1.0 / it.v)
	} }
	/** Returns the length of this line segment */
	fun length(): Double {
		val y = b.y.v - a.y.v
		val x = b.x.v - a.x.v
		return sqrt((y * y) + (x * x))
	}
	/* Returns the angle between X axis and this line, but transformed so that the origin is point A */
	fun atan2() = atan2(b.y.v - a.y.v, b.x.v - a.x.v)
	/* Returns the midpoint of this line segment */
	fun midpoint() = P((a.x.v + b.x.v) / 2.0, (a.y.v + b.y.v) / 2.0)
	/** Translates this line edge so that point A is at the origin */
	fun vector() = Edge(P(0.0, 0.0), Point(b.x - a.x, b.y - a.y))
	/** Returns an Edge extended by `distanec` in the direction of A -> B */
	fun extend(distance: Double): Edge {
		//Vertical lines
		if (a.x == b.x)
			return if (b.y > a.y) Edge(a, Point(b.x, F64U(b.y.v + distance)))
				else Edge(a, Point(b.x, F64U(b.y.v - distance)))
		//Horizontal lines
		if (a.y == b.y)
			return if (b.x > a.x) Edge(a, Point(F64U(b.x.v + distance), b.y))
				else Edge(a, Point(F64U(b.x.v - distance), b.y))
		//Other lines
		val vector = vector()
		val vector_length = vector.length()
		return Edge(a, Point(b.x + (vector.b.x / vector_length * distance), b.y + (vector.b.y / vector_length * distance)))
	}
	/** Returns the point at `percent`% of this line segment from a -> b. Percent is clamped [0, 1] */
	fun percent_point(percent: Double): Point<F64Unit> {
		var actual_percent = percent
		if (actual_percent > 1.0) actual_percent = 1.0
		if (actual_percent < 0.0) actual_percent = 0.0
		return Point(F64U(actual_percent) * (b.x - a.x) + a.x, F64U(actual_percent) * (b.y - a.y) + a.y)
	}

	/** Lists the possible points where this Edge and the other Edge could be cut to stop overlapping */
	fun should_cut_at_with_endpoints(other: Edge): List<Point<F64Unit>>? {

		val (a1, a2) = this
		val (b1, b2) = other
		val a_slope = this.slope()
		val b_slope = other.slope()

		//
		// Same slope:
		// The cuts will always be at the endpoints
		//

		//Two vertical lines
		if (a_slope.is_yesnt() && b_slope.is_yesnt()) {
			if (a1.x == b1.x) {
				val cut_at = mutableListOf<Point<F64Unit>>()

				if (a1.y < a2.y) {
					if (b1.y > a1.y && b1.y < a2.y) cut_at.add(b1)
					if (b2.y > a1.y && b2.y < a2.y) cut_at.add(b2)
				} else {
					if (b1.y > a2.y && b1.y < a1.y) cut_at.add(b1)
					if (b2.y > a2.y && b2.y < a1.y) cut_at.add(b2)
				}

				if (b1.y < b2.y) {
					if (a1.y > b1.y && a1.y < b2.y) cut_at.add(a1)
					if (a2.y > b1.y && a2.y < b2.y) cut_at.add(a2)
				} else {
					if (a1.y > b2.y && a1.y < b1.y) cut_at.add(a1)
					if (a2.y > b2.y && a2.y < b1.y) cut_at.add(a2)
				}

				return if (cut_at.isNotEmpty()) cut_at else null
			}
			else return null
		}

		//Two horizontal lines
		//Same as vertical lines, but swapped
		if (a_slope.isZero() && b_slope.isZero()) {
			if (a1.y == b1.y) {
				val cut_at = mutableListOf<Point<F64Unit>>()

				if (a1.x < a2.x) {
					if (b1.x > a1.x && b1.x < a2.x) cut_at.add(b1)
					if (b2.x > a1.x && b2.x < a2.x) cut_at.add(b2)
				} else {
					if (b1.x > a2.x && b1.x < a1.x) cut_at.add(b1)
					if (b2.x > a2.x && b2.x < a1.x) cut_at.add(b2)
				}

				if (b1.x < b2.x) {
					if (a1.x > b1.x && a1.x < b2.x) cut_at.add(a1)
					if (a2.x > b1.x && a2.x < b2.x) cut_at.add(a2)
				} else {
					if (a1.x > b2.x && a1.x < b1.x) cut_at.add(a1)
					if (a2.x > b2.x && a2.x < b1.x) cut_at.add(a2)
				}

				return if (cut_at.isNotEmpty()) cut_at else null
			}
			else return null
		}


		//Same-slope and potentially overlapping
		if (a_slope == b_slope) {
			val aligned = ((b1.y - a1.y) / (b1.x - a1.x)).v.absoluteValue == a_slope.v.absoluteValue
			if (aligned) {
				val cut_at = mutableListOf<Point<F64Unit>>()

				if (a1.x < a2.x) {
					if (b1.x > a1.x && b1.x < a2.x) cut_at.add(b1)
					if (b2.x > a1.x && b2.x < a2.x) cut_at.add(b2)
				} else {
					if (b1.x > a2.x && b1.x < a1.x) cut_at.add(b1)
					if (b2.x > a2.x && b2.x < a1.x) cut_at.add(b2)
				}

				if (b1.x < b2.x) {
					if (a1.x > b1.x && a1.x < b2.x) cut_at.add(a1)
					if (a2.x > b1.x && a2.x < b2.x) cut_at.add(a2)
				} else {
					if (a1.x > b2.x && a1.x < b1.x) cut_at.add(a1)
					if (a2.x > b2.x && a2.x < b1.x) cut_at.add(a2)
				}

				return if (cut_at.isNotEmpty()) cut_at else null
			}
			else return null
		}

		// 
		// Special case: Horizontal + Vertical
		// There will be one intersection point
		//

		//One horizontal and one vertical
		if (a_slope.isZero() && b_slope.is_yesnt())
			return if (
				(
					if (a1.x <= a2.x) b1.x >= a1.x && b1.x <= a2.x
					else b1.x >= a2.x && b1.x <= a1.x
				)
				&&
				(
					if (b1.y <= b2.y) a1.y >= b1.y && a1.y <= b2.y
					else b1.y >= a2.y && b1.y <= a1.y
				)
			)
			listOf(Point(b1.x, a1.y))
			else 
			null

		//Same, but the other way around
		if (b_slope.isZero() && a_slope.is_yesnt())
			return if (
				(
					if (b1.x <= b2.x) a1.x >= b1.x && a1.x <= b2.x
					else a1.x >= b2.x && a1.x <= b1.x
				)
				&&
				(
					if (a1.y <= a2.y) b1.y >= a1.y && b1.y <= a2.y
					else a1.y >= b2.y && a1.y <= b1.y
				)
			)
			listOf(Point(a1.x, b1.y))
			else 
			null

		// 
		// Special case: Vertical + General Line
		//
		if (a_slope.is_yesnt()) {
			//A is the vertical line, B is the general line
			//IY is the Y value along the line from the first point of B to the intersection with A
			val iy = b_slope * (a1.x - b1.x) + b1.y
			val colliding = (
				if (b1.x < b2.x) a1.x >= b1.x && a1.x <= b2.x
				else a1.x >= b2.x && a1.x <= b1.x
			) && (
				if (a1.y < a2.y) iy >= a1.y && iy <= a2.y
				else iy >= a2.y && iy <= a1.y
			)
			return if (colliding) listOf(Point(a1.x, iy)) else null
		}
		else if (b_slope.is_yesnt()) {
			val iy = a_slope * (b1.x - a1.x) + a1.y
			val colliding = (
				if (a1.x < a2.x) b1.x >= a1.x && b1.x <= a2.x
				else b1.x >= a2.x && b1.x <= a1.x
			) && (
				if (b1.y < b2.y) iy >= b1.y && iy <= b2.y
				else iy >= b2.y && iy <= b1.y
			)
			return if (colliding) listOf(Point(b1.x, iy)) else null
		}

		//
		// General Line Segments
		//

		val intersection_x = 
			(
				(b_slope * b1.x)
				-
				(a_slope * a1.x)
				+
				a1.y
				-
				b1.y
			) / (
				b_slope - a_slope
			)
		val ix = intersection_x

		val colliding = (
			if (a1.x <= a2.x) ix >= a1.x && ix <= a2.x
			else ix >= a2.x && ix <= a1.x
		) && (
			if (b1.x <= b2.x) ix >= b1.x && ix <= b2.x
			else ix >= b2.x && ix <= b1.x
		)

		val iy = a_slope * (ix - a1.x) + a1.y
		if (colliding) return listOf(Point(ix, iy))
		else return null
	}

	/** Lists the possible points where this Edge and the other Edge could be cut to stop overlapping
	 *  @param other - the edge to check for intersection with
	 *  @return a list of points to cut at, or null if the edges did not intersect */
	//Except that this filters out endpoints from the results, since a conneciton of two edges is not an overlap
	fun should_cut_at(other: Edge) = should_cut_at_with_endpoints(other)
		?.filter { (it != a && it != b) || (it != other.a && it != other.b) }
		?.let { if (it.isEmpty()) null else it }

	/** Tests if an edge intersects with this edge, and if it does, returns a list of new edges that should replace both edges
	 * @param other - the edge to check for intersection with
	 * @return a list of new edges to replace both `this` and `other`, or null if the edges do not intersect */
	fun cut_with(other: Edge): List<Edge>? {
		val a = this
		val b = other
		return a.should_cut_at(b)?.let {
			val new_edges = 

				//Only 1 cut point? Simple cut
				if (it.size == 1) {
					val p = it[0]
					listOf(
						//Return edges from each endpoint to the cut point
						Edge(a.a, p),
						Edge(a.b, p),
						Edge(b.a, p),
						Edge(b.b, p)
					)
				}

				//2 cut points: the segments have the same slope
				//Return 3 edges from the 2 edges, the new edge being inbetween the two cut points
				else if (it.size == 2) {
					val points = mutableSetOf(
						a.a, a.b,
						b.a, b.b,
						*it.toTypedArray()
					)
					//Pick the farthest out point to start iteration from
					var iter = 
						//Vertical lines
						if (a.a.x == a.b.x)  points.minBy { it.y.v }
						//Horizontal or Sloped lines
						else points.minBy { it.x.v }
					points.remove(iter)

					//Iterate until there are no points left, creating Edges from the sequence of iteration
					val new_edges = mutableListOf<Edge>()
					while (points.isNotEmpty()) {
						//The next point in the chain is the closest to the iterator
						val other = points.minBy { it.distance_to(iter) }
						points.remove(other)
						new_edges.add(Edge(iter, other))
						iter = other
					}
					new_edges
				}
				else { throw Exception("How did we get here?") }

			//If an edge's endpoints are equal, it isn't an edge, it's a point
			return new_edges.filter { it.a != it.b }
		}

	}
}

private data class OrderedTriangle(val a: Point<F64Unit>, val b: Point<F64Unit>, val c: Point<F64Unit>) {}
data class Triangle(val a: Point<F64Unit>, val b: Point<F64Unit>, val c: Point<F64Unit>) {
	private fun ordered(): OrderedTriangle {
		var a = this.a
		var b = this.b
		var c = this.c
		if (
			c.x < b.x ||
			(c.x == b.x && c.y < b.y)
		) {
			val s = b
			b = c
			c = s
		}
		if (
			b.x < a.x ||
			(b.x == a.x && b.y < a.y)
		) {
			val s = a
			a = b
			b = s
		}
		if (
			b.x > c.x ||
			(b.x == c.x && b.y > c.y)
		) {
			val s = b
			b = c
			c = s
		}
		return OrderedTriangle(a, b, c)
	}
	override fun equals(other: Any?): Boolean {
		return if (other is Triangle) this.ordered() == other.ordered() else false
	}
	override fun hashCode() = this.ordered().hashCode()

	fun points() = arrayOf(a, b, c)

	fun centroid(): Point<F64Unit> {
		return P((a.x.v + b.x.v + c.x.v) / 3.0, (a.y.v + b.y.v + c.y.v) / 3.0)
		/*
		//      C
		//     /  \
		//  b /    \  a
		//  /      \
		// A ------ B
		//     c
		val edge_c = Edge(a, b)
		val edge_a = Edge(b, c)
		val edge_b = Edge(c, a)
		val median_a = Edge(a, edge_a.midpoint())
		val median_b = Edge(b, edge_b.midpoint())
		val median_c = Edge(c, edge_c.midpoint())
		val intersection_ab = median_a.should_cut_at(median_b)
		val intersection_bc = median_b.should_cut_at(median_c)
		if (intersection_ab!![0] != intersection_bc!![0]) throw RuntimeException("Triangle centroid is ka-put")
		return intersection_ab[0]
		//val tsa = edge_a.tangent_slope()
		//val tsb = edge_b.tangent_slope()
		//val tsc = edge_c.tangent_slope()

		/*
		//Calculates the angle from the X axis to the line that bisects the angle fromed from the two edges
		//In the range [0, 2Pi)
		fun triangle_angle(e1: Edge, e2: Edge): Double {
			var a1 = e1.atan2()
			while (a1 < 0.0) a1 += PI + PI
			var a2 = e2.atan2()
			while (a2 < 0.0) a2 += PI + PI
			var angle = (a1 + a2) / 2.0
			return angle
		}
		val angle_a = triangle_angle(edge_c, edge_b)
		val angle_b = triangle_angle(edge_a, edge_c)
		val angle_c = triangle_angle(edge_b, edge_a)
		val slope_a = tan(angle_a)
		val slope_b = tan(angle_b)
		val slope_c = tan(angle_c)*/ */
	}
}

