package dev.alador.mechanical_quill

sealed interface Color {}

data class Color8888(val r: UByte, val g: UByte, val b: UByte, val a: UByte): Color {
	companion object {
		fun from(color: Color): Color8888 {
			if (color is Color8888) return color
			else if (color is Color565) return Color8888(
				(color.r.toDouble() / Color565.RED_BLUE_CAP.toDouble() * 255.0).toInt().toUByte(),
				(color.g.toDouble() / Color565.GREEN_CAP.toDouble() * 255.0).toInt().toUByte(),
				(color.b.toDouble() / Color565.RED_BLUE_CAP.toDouble() * 255.0).toInt().toUByte(),
				255u.toUByte(),
			)
			else if (color is ColorF32) return Color8888(
				(color.r * 255.0).toInt().toUByte(),
				(color.g * 255.0).toInt().toUByte(),
				(color.b * 255.0).toInt().toUByte(),
				(color.a * 255.0).toInt().toUByte(),
			)
			else throw RuntimeException("Unknown color type")
		}
	}
}

data class Color565(val r: UByte, val g: UByte, val b: UByte): Color {
	init {
		if (r > RED_BLUE_CAP) throw ColorOutOfBoundsException("Red was not within RED_BLUE_CAP")
		if (g > GREEN_CAP) throw ColorOutOfBoundsException("Green was not within GREEN_CAP")
		if (b > RED_BLUE_CAP) throw ColorOutOfBoundsException("Blue was not within RED_BLUE_CAP")
	}

	companion object {
		val RED_BLUE_CAP = 0b00011111u.toUByte()
		val GREEN_CAP = 0b00111111u.toUByte()

		fun from(color: Color): Color565 {
			if (color is Color8888) return Color565(
				(color.r.toDouble() / 255.0 * RED_BLUE_CAP.toDouble()).toInt().toUByte(),
				(color.g.toDouble() / 255.0 * GREEN_CAP.toDouble()).toInt().toUByte(),
				(color.b.toDouble() / 255.0 * RED_BLUE_CAP.toDouble()).toInt().toUByte(),
			)
			else if (color is Color565) return color
			else if (color is ColorF32) return Color565(
				(color.r * Color565.RED_BLUE_CAP.toDouble()).toInt().toUByte(),
				(color.g * Color565.GREEN_CAP.toDouble()).toInt().toUByte(),
				(color.b * Color565.RED_BLUE_CAP.toDouble()).toInt().toUByte(),
			)
			else throw RuntimeException("Unknown color type")
		}
	}
}

data class ColorF32(val r: Float, val g: Float, val b: Float, val a: Float): Color {
	init {
		if (r < 0f || r > 1f) throw ColorOutOfBoundsException("Red was not within [0.0, 1.0]")
		if (g < 0f || g > 1f) throw ColorOutOfBoundsException("Green was not within [0.0, 1.0]")
		if (b < 0f || b > 1f) throw ColorOutOfBoundsException("Blue was not within [0.0, 1.0]")
		if (a < 0f || a > 1f) throw ColorOutOfBoundsException("Alpha was not within [0.0, 1.0]")
	}

	companion object {
		fun from(color: Color): ColorF32 {
			if (color is Color8888) return ColorF32(
				color.r.toFloat() / 255f,
				color.g.toFloat() / 255f,
				color.b.toFloat() / 255f,
				color.a.toFloat() / 255f,
			)
			else if (color is Color565) return ColorF32(
				color.r.toFloat() / Color565.RED_BLUE_CAP.toFloat(),
				color.g.toFloat() / Color565.GREEN_CAP.toFloat(),
				color.b.toFloat() / Color565.RED_BLUE_CAP.toFloat(),
				1f
			)
			else if (color is ColorF32) return color
			else throw RuntimeException("Unknown color type")
		}
	}
}

class ColorOutOfBoundsException(msg: String) : RuntimeException(msg)
