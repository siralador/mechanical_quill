package dev.alador.mechanical_quill

import kotlin.jvm.JvmInline
import kotlin.math.sqrt

import dev.alador.mechanical_quill.util.FancyPrinter
import dev.alador.mechanical_quill.util.print_document_bits

enum class ColorEncoding {
	RGBA_8888,
	RGB_565,
	RGBA_F32,
}

enum class CoordinateRange {
	Default,
	Reduced,
	Enhanced
}

data class HeaderData<U: UnitType>(
	val scale: UByte,
	val color_encoding: ColorEncoding,
	val coordinate_range: CoordinateRange,
	val width: UInt,
	val height: UInt,
	val color_count: UInt
) {}

sealed interface UnitType {
	fun as_f64(scale: UByte): F64Unit
}

@JvmInline
value class RawUnit(val v: Int): UnitType {
	override fun as_f64(scale: UByte): F64Unit = F64Unit(v.toDouble() / (0b1 shl scale.toInt()).toDouble())
	override fun toString(): String = FancyPrinter.CURRENT?.raw_unit_scale?.let { as_f64(it).toString() } ?: "RAW(${v})"
}

@JvmInline
value class F64Unit(val v: Double): UnitType {
	override fun as_f64(scale: UByte): F64Unit = this
	fun as_raw(scale: UByte): RawUnit = RawUnit((v * (0b1 shl scale.toInt())).toInt())
	override fun toString() = v.toString()

	operator fun compareTo(other: F64Unit) = v.compareTo(other.v)
	operator fun compareTo(other: Double) = v.compareTo(other)
	operator fun plus(other: F64Unit) = F64Unit(v + other.v)
	operator fun plus(other: Double) = F64Unit(v + other)
	operator fun minus(other: F64Unit) = F64Unit(v - other.v)
	operator fun minus(other: Double) = F64Unit(v - other)
	operator fun times(other: F64Unit) = F64Unit(v * other.v)
	operator fun times(other: Double) = F64Unit(v * other)
	operator fun div(other: F64Unit) = F64Unit(v / other.v)
	operator fun div(other: Double) = F64Unit(v / other)
	fun isNaN() = v.isNaN()
	fun isInfinite() = v.isInfinite()
	fun isZero() = v == 0.0 || v == -0.0
}
fun F64U(v: Double) = F64Unit(v)
fun F64U(v: F64Unit) = F64Unit(v.v) 

data class Point<U: UnitType>(val x: U, val y: U) {
	fun as_f64(scale: UByte): Point<F64Unit> = Point(x.as_f64(scale), y.as_f64(scale))
}
fun P(x: Double, y: Double) = Point(F64U(x), F64U(y))
fun Point<F64Unit>.distance_to(other: Point<F64Unit>): Double {
	val xs = x.v - other.x.v
	val ys = y.v - other.y.v
	return sqrt((xs * xs) + (ys * ys))
}

data class Rectangle<U: UnitType>(val origin: Point<U>, val width: U, val height: U) {
	fun as_f64(scale: UByte): Rectangle<F64Unit> = Rectangle(origin.as_f64(scale), width.as_f64(scale), height.as_f64(scale))
}

data class Line<U: UnitType>(val start: Point<U>, val end: Point<U>) {
	fun as_f64(scale: UByte): Line<F64Unit> = Line(start.as_f64(scale), end.as_f64(scale))
}




enum class StyleKind {
	Flat,
	LinearGradient,
	RadialGradient,
}

sealed interface Style<U: UnitType> {
	fun as_f64(scale: UByte): Style<F64Unit>
}
fun<S: Style<UnitType>> S.as_f64(scale: UByte): S = as_f64(scale) as S

data class FlatStyle<U: UnitType>(val color_index: UInt): Style<U> {
	override fun as_f64(scale: UByte): Style<F64Unit> = FlatStyle(color_index)
}

data class LinearGradientStyle<U: UnitType>(
	val point_0: Point<U>,
	val color_index_0: UInt,
	val point_1: Point<U>,
	val color_index_1: UInt,
): Style<U> {
	override fun as_f64(scale: UByte): Style<F64Unit> = LinearGradientStyle(point_0.as_f64(scale), color_index_0, point_1.as_f64(scale), color_index_1)
}

data class RadialGradientStyle<U: UnitType>(
	val point_0: Point<U>,
	val color_index_0: UInt,
	val point_1: Point<U>,
	val color_index_1: UInt,
): Style<U> {
	override fun as_f64(scale: UByte): Style<F64Unit> = RadialGradientStyle(point_0.as_f64(scale), color_index_0, point_1.as_f64(scale), color_index_1)
}




sealed interface Command<U: UnitType> {
	fun clone_as_f64(scale: UByte): Command<F64Unit>
	fun fancy_print(p: FancyPrinter): Unit
}

class EndOfDocumentCommand<U: UnitType>(): Command<U> {
	override fun clone_as_f64(scale: UByte) = EndOfDocumentCommand<F64Unit>()
	override fun fancy_print(p: FancyPrinter) { p.println("End Of Document") }
}

data class FillPolygonCommand<U: UnitType>(
	val points: List<Point<U>>,
	val style: Style<U>,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = FillPolygonCommand(points.map { it.as_f64(scale) }, style.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Fill Polygon with ${style}:")
		p.tab += 1u
		for ((i, point) in points.iterator().withIndex()) {
			p.println("${i}: ${point}")
		}
		p.tab -= 1u
	}
}

data class FillRectanglesCommand<U: UnitType>(
	val rectangles: List<Rectangle<U>>,
	val style: Style<U>,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = FillRectanglesCommand(rectangles.map { it.as_f64(scale) }, style.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Fill Rectangles with ${style}:")
		p.tab += 1u
		for ((i, rectangle) in rectangles.iterator().withIndex()) {
			p.println("${i}: ${rectangle}")
		}
		p.tab -= 1u
	}
}

data class FillPathCommand<U: UnitType>(
	val path: Path<U>,
	val style: Style<U>,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = FillPathCommand(path.clone_as_f64(scale), style.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Fill Path with ${style}:")
		p.tab += 1u
		path.fancy_print(p)
		p.tab -= 1u
	}
}

data class DrawLinesCommand<U: UnitType>(
	val lines: List<Line<U>>,
	val style: Style<U>,
	val width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = DrawLinesCommand(lines.map { it.as_f64(scale) }, style.as_f64(scale), width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Outline Lines of width ${width} with ${style}:")
		p.tab += 1u
		for ((i, line) in lines.iterator().withIndex()) {
			p.println("${i}: ${line}")
		}
		p.tab -= 1u
	}
}

data class DrawLineLoopCommand<U: UnitType>(
	val points: List<Point<U>>,
	val style: Style<U>,
	val width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = DrawLineLoopCommand(points.map { it.as_f64(scale) }, style.as_f64(scale), width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Outline Line Loop of width ${width} with style ${style}:")
		p.tab += 1u
		for ((i, point) in points.iterator().withIndex()) {
			p.println("${i}: ${point}")
		}
		p.tab -= 1u
	}
}

data class DrawLineStripCommand<U: UnitType>(
	val points: List<Point<U>>,
	val style: Style<U>,
	val width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = DrawLineLoopCommand(points.map { it.as_f64(scale) }, style.as_f64(scale), width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Outline Line Strip of width ${width} with style ${style}:")
		p.tab += 1u
		for ((i, point) in points.iterator().withIndex()) {
			p.println("${i}: ${point}")
		}
		p.tab -= 1u
	}
}

data class DrawLinePathCommand<U: UnitType>(
	val path: Path<U>,
	val style: Style<U>,
	val width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = DrawLinePathCommand(path.clone_as_f64(scale), style.as_f64(scale), width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Outline Path of width ${width} with style ${style}:")
		p.tab += 1u
		path.fancy_print(p)
		p.tab -= 1u
	}
}

data class OutlineFillPolygonCommand<U: UnitType>(
	val points: List<Point<U>>,
	val fill_style: Style<U>,
	val line_style: Style<U>,
	val line_width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = OutlineFillPolygonCommand(points.map { it.as_f64(scale) }, fill_style.as_f64(scale), line_style.as_f64(scale), line_width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Draw Path filled with ${fill_style}, outlined in ${line_style} width ${line_width}")
		p.tab += 1u
		for ((i, point) in points.iterator().withIndex()) {
			p.println("${i}: ${point}")
		}
		p.tab -= 1u
	}
}

data class OutlineFillRectanglesCommand<U: UnitType>(
	val rectangles: List<Rectangle<U>>,
	val fill_style: Style<U>,
	val line_style: Style<U>,
	val line_width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = OutlineFillRectanglesCommand(rectangles.map { it.as_f64(scale) }, fill_style.as_f64(scale), line_style.as_f64(scale), line_width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Draw Rectangles filled with ${fill_style}, outlined in ${line_style} width ${line_width}")
		p.tab += 1u
		for ((i, rectangle) in rectangles.iterator().withIndex()) {
			p.println("${i}: ${rectangle}")
		}
		p.tab -= 1u
	}
}

data class OutlineFillPathCommand<U: UnitType>(
	val path: Path<U>,
	val fill_style: Style<U>,
	val line_style: Style<U>,
	val line_width: U,
): Command<U> {
	override fun clone_as_f64(scale: UByte) = OutlineFillPathCommand(path.clone_as_f64(scale), fill_style.as_f64(scale), line_style.as_f64(scale), line_width.as_f64(scale))
	override fun fancy_print(p: FancyPrinter) {
		p.println("Draw Path filled with ${fill_style}, outlined in ${line_style} width ${line_width}")
		p.tab += 1u
		path.fancy_print(p)
		p.tab -= 1u
	}
}







data class Path<U: UnitType>(val segments: List<PathSegment<U>>) {
	fun clone_as_f64(scale: UByte) = Path(segments.map { it.clone_as_f64(scale) })
	fun fancy_print(p: FancyPrinter) {
		for ((i, segment) in segments.iterator().withIndex()) {
			p.println("Segment ${i}:")
			p.tab += 1u
			segment.fancy_print(p)
			p.tab -= 1u
		}
	}
}

data class PathSegment<U: UnitType>(val start: Point<U>, val actions: List<PathAction<U>>) {
	fun clone_as_f64(scale: UByte) = PathSegment(start.as_f64(scale), actions.map { it.as_f64(scale) })
	fun fancy_print(p: FancyPrinter) {
		p.println("Start: ${start}")
		for ((i, action) in actions.iterator().withIndex()) {
			p.println("${i}: ${action}")
		}
	}
}

sealed interface PathAction<U: UnitType> {
	fun as_f64(scale: UByte): PathAction<F64Unit>
}
fun<PA: PathAction<UnitType>> PA.as_f64(scale: UByte): PA = as_f64(scale) as PA

data class LineAction<U: UnitType>(val end: Point<U>): PathAction<U> {
	override fun as_f64(scale: UByte) = LineAction(end.as_f64(scale))
}
data class HorizontalAction<U: UnitType>(val end_x: U): PathAction<U> {
	override fun as_f64(scale: UByte) = HorizontalAction(end_x.as_f64(scale))
}
data class VerticalAction<U: UnitType>(val end_y: U): PathAction<U> {
	override fun as_f64(scale: UByte) = HorizontalAction(end_y.as_f64(scale))
}

data class CubicBezierAction<U: UnitType>(
	val control_0: Point<U>,
	val control_1: Point<U>,
	val end: Point<U>,
): PathAction<U> {
	override fun as_f64(scale: UByte) = CubicBezierAction(control_0.as_f64(scale), control_1.as_f64(scale), end.as_f64(scale))
}

data class ArcCircleAction<U: UnitType>(
	val radius: U,
	val end: Point<U>,
	val large_arc: Boolean,
	val sweep: Boolean,
): PathAction<U> {
	override fun as_f64(scale: UByte) = ArcCircleAction(radius.as_f64(scale), end.as_f64(scale), large_arc, sweep)
}

data class ArcEllipseAction<U: UnitType>(
	val radius_x: U,
	val radius_y: U,
	val end: Point<U>,
	val rotation: U,
	val large_arc: Boolean,
	val sweep: Boolean,
): PathAction<U> {
	override fun as_f64(scale: UByte) = ArcEllipseAction(radius_x.as_f64(scale), radius_y.as_f64(scale), end.as_f64(scale), rotation.as_f64(scale), large_arc, sweep)
}

class ClosePathAction<U: UnitType>(): PathAction<U> {
	override fun as_f64(scale: UByte) = ClosePathAction<F64Unit>()
	override fun toString() = "ClosePathAction"
}

data class QuadraticBezierAction<U: UnitType>(
	val control: Point<U>,
	val end: Point<U>,
): PathAction<U> {
	override fun as_f64(scale: UByte) = QuadraticBezierAction(control.as_f64(scale), end.as_f64(scale))
}



class Document<U: UnitType, C: Color>(
	var header: HeaderData<U>,
	var color_table: MutableList<C>,
	var commands: MutableList<Command<U>>,
) {
	override fun toString(): String {
		val builder = StringBuilder()
		val printer = FancyPrinter(
			print_func = { dat -> builder.append(dat) }
		)
		print_document_bits(
			header as HeaderData<UnitType>?,
			color_table,
			commands as List<Command<UnitType>>,
			printer
		)
		return builder.toString()
	}
}
