package dev.alador.mechanical_quill.util

import dev.alador.mechanical_quill.Color
import dev.alador.mechanical_quill.Command
import dev.alador.mechanical_quill.UnitType
import dev.alador.mechanical_quill.HeaderData

expect class ByteCasting() {
	fun as_float(b1: UByte, b2: UByte, b3: UByte, b4: UByte): Float
}

typealias PrintFunc = (dat: String) -> Unit
class FancyPrinter(
	var tab: UInt = 0u,
	val print_func: PrintFunc = { dat -> print(dat) },
	var raw_unit_scale: UByte? = null,
) {
	private var has_tabbed = false
	private fun print_tab() {
		for (_i in 1u..tab) print_func("\t")
		has_tabbed = true
	}

	fun print(dat: Any?) {
		if (!has_tabbed) print_tab()
		val string_dat = dat?.toString() ?: "null"
		var i = 0
		var newline = string_dat.indexOf("\n", i)
		while (newline >= 0 && newline < string_dat.length - 1) {
			print_func(string_dat.substring(i, newline))
			print_tab()
			i = newline + 1
			newline = string_dat.indexOf("\n", i)
		}
		if (newline == string_dat.length - 1) has_tabbed = false
		print_func(string_dat.substring(i))
	}
	fun println(dat: Any?) {
		print(dat)
		print("\n")
	}

	companion object {
		var CURRENT: FancyPrinter? = null
			private set
	}
	fun context(block: () -> Unit) {
		val old = CURRENT
		CURRENT = this
		block()
		CURRENT = old
	}
}

fun print_document_bits(
	header: HeaderData<UnitType>?,
	color_table: List<Color>,
	commands: List<Command<UnitType>>,
	p: FancyPrinter = FancyPrinter()
) {
	val old_raw_unit_scale = p.raw_unit_scale
	p.raw_unit_scale = header?.scale
	p.context {
		p.println("Header: ${header}")
		p.println("Color table:")
		p.tab += 1u
		for ((i, color) in color_table.iterator().withIndex()) {
			p.println("${i}: ${color}")		
		}
		p.tab -= 1u
		p.println("Commands: ")
		p.tab += 1u
		for (command in commands) {
			command.fancy_print(p)				
		}
	}
	p.raw_unit_scale = old_raw_unit_scale
}
