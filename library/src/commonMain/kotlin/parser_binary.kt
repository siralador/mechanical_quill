package dev.alador.mechanical_quill.parser

import kotlin.collections.ArrayDeque
import kotlin.collections.ArrayList

import dev.alador.mechanical_quill.*
import dev.alador.mechanical_quill.util.ByteCasting
import dev.alador.mechanical_quill.util.FancyPrinter
import dev.alador.mechanical_quill.util.print_document_bits

class BinaryParsingEngine {
	private val cast = ByteCasting()

	private var slices: ArrayDeque<ByteArray> = ArrayDeque()
	private var slices_cleaned_up = false
	private var i = 0

	var finished: Boolean = false
		private set
	var has_trailing_data = false
		private set
	fun input(data: ByteArray): Boolean {
		slices.add(data)
		try {
			if (engine.next()) {
				//Finished!!!!
				finished = true
				if (slices.isNotEmpty() && i < slices[0].size) {
					has_trailing_data = true
				} else {
					//Drop allocations
					slices = ArrayDeque(0)
				}
				return true
			} else {
				return false
			}
		} catch (e: Throwable) {
			finished = true
			throw e
		}
	}


	var header: HeaderData<RawUnit>? = null
		private set
	val color_table = mutableListOf<Color>()
	val commands = mutableListOf<Command<RawUnit>>()

	var document: Document<RawUnit, Color>? = null
		private set

	//Returns true when the document is fully parsed, otherwise yields false
	private val engine: Iterator<Boolean> = sequence<Boolean> {

		//The heart of the engine: returns the next byte, or yields, relying on the caller to insert more bytes into `slices`
		suspend fun SequenceScope<Boolean>.read_ubyte(): UByte {
			while (slices.size <= 0 || i >= slices[0].size) {
				i = 0
				slices.removeFirstOrNull()
				yield(false)
			}
			val byte = slices[0][i];
			i += 1
			return byte.toUByte()
		}

		//Supplimentary reading functions
		suspend fun SequenceScope<Boolean>.read_ushort(): UShort {
			val byte1 = read_ubyte().toUInt()
			val byte2 = read_ubyte().toUInt()
			return (byte1 or (byte2 shl 8)).toUShort()
		}
		suspend fun SequenceScope<Boolean>.read_uint(): UInt {
			val byte1 = read_ubyte().toUInt()
			val byte2 = read_ubyte().toUInt()
			val byte3 = read_ubyte().toUInt()
			val byte4 = read_ubyte().toUInt()
			return (byte1 or (byte2 shl 8) or (byte3 shl 16) or (byte4 shl 24))
		}
		suspend fun SequenceScope<Boolean>.read_varuint(): UInt {
			var byte = read_ubyte().toUInt()
			var result = byte and 0b01111111u
			var shift = 7
			while (byte and 0b10000000u > 0u) {
				byte = read_ubyte().toUInt()
				result = result or ((byte and 0b01111111u) shl shift)
				shift += 7
			}
			return result
		}
		suspend fun SequenceScope<Boolean>.read_float(): Float {
			return cast.as_float(read_ubyte(), read_ubyte(), read_ubyte(), read_ubyte())
		}

		//Convienience functions
		fun die(msg: String) { throw ParsingException(msg, this@BinaryParsingEngine) }
		fun<T> diet(msg: String): T { throw ParsingException(msg, this@BinaryParsingEngine) }

		//
		// HEADER
		//

		//Magic
		if (read_ubyte().toUInt() != 0x72u || read_ubyte().toUInt() != 0x56u) die("TinyVG magic identifier was not present")
		//Version
		if (read_ubyte().toUInt() != 1u) die("TinyVG version was not 1")

		//Scale, color encoding, and coordinate range
		val pack = read_ubyte().toUInt()
		val scale = pack and 0b00001111u
		val raw_color_encoding = (pack and 0b00110000u) shr 4
		val raw_coordinate_range = (pack and 0b11000000u) shr 6

		val color_encoding = when(raw_color_encoding) {
			0u -> ColorEncoding.RGBA_8888
			1u -> ColorEncoding.RGB_565
			2u -> ColorEncoding.RGBA_F32
			else -> diet("Unsupported color encoding")
		}
		
		val coordinate_range = when(raw_coordinate_range) {
			0u -> CoordinateRange.Default
			1u -> CoordinateRange.Reduced
			2u -> CoordinateRange.Enhanced
			else -> diet("Unsupported coordinate range")
		}

		//Reads either a u8, u16, or a u32 depending on the value of coordinate_range
		suspend fun SequenceScope<Boolean>.read_unit(): RawUnit {
			return RawUnit(when (coordinate_range) {
				CoordinateRange.Default -> read_ushort().toShort().toInt()
				CoordinateRange.Reduced -> read_ubyte().toByte().toInt()
				CoordinateRange.Enhanced -> read_uint().toInt()
			})
		}
		suspend fun SequenceScope<Boolean>.read_point(): Point<RawUnit> = Point(read_unit(), read_unit())

		//Rest of the header
		val width = read_unit().v.toUInt()
		val height = read_unit().v.toUInt()
		val color_count = read_varuint()

		header = HeaderData(scale.toUByte(), color_encoding, coordinate_range, width, height, color_count)

		//
		// Color Table
		//
		
		when (color_encoding) {

			ColorEncoding.RGBA_8888 -> {
				for (_i in 1u..color_count) {
					color_table.add(Color8888(read_ubyte(), read_ubyte(), read_ubyte(), read_ubyte()))															
				}
			}

			ColorEncoding.RGB_565 -> {
				for (_i in 1u..color_count) {
					val buf = read_ushort().toUInt()
					val red = buf and 0b0000000000011111u
					val green = (buf and 0b0000011111100000u) shr 5
					val blue = (buf and 0b1111100000000000u) shr 11
					color_table.add(Color565(red.toUByte(), green.toUByte(), blue.toUByte()))
				}
			}

			ColorEncoding.RGBA_F32 -> {
				for (_i in 1u..color_count) {
					color_table.add(ColorF32(read_float(), read_float(), read_float(), read_float()))
				}
			}

		}

		//
		// Style
		//

		suspend fun SequenceScope<Boolean>.read_style(raw_style_kind: UInt): Style<RawUnit> {
			return when (raw_style_kind) {
				//Flat
				0u -> FlatStyle(read_varuint())

				//Linear Gradient
				1u -> {
					val point_0 = read_point()
					val point_1 = read_point()
					val color_index_0 = read_varuint()
					val color_index_1 = read_varuint()
					LinearGradientStyle(point_0, color_index_0, point_1, color_index_1)
				}

				//Radial Gradient
				2u -> {
					val point_0 = read_point()
					val point_1 = read_point()
					val color_index_0 = read_varuint()
					val color_index_1 = read_varuint()
					RadialGradientStyle(point_0, color_index_0, point_1, color_index_1)
				}

				else -> diet("Unknown style kind")
			}
		}

		//
		// Paths
		//

		suspend fun SequenceScope<Boolean>.read_path(num_segments: UInt, segments: MutableList<PathSegment<RawUnit>>) {
			val action_counts = mutableListOf<UInt>()
			for (_i in 1u..num_segments) action_counts.add(read_varuint() + 1u)
			for (num_actions in action_counts) {
				val start = read_point()
				val actions = mutableListOf<PathAction<RawUnit>>()
				segments.add(PathSegment(start, actions))

				for (_a in 1u..num_actions) {
					val pack = read_ubyte().toUInt()
					val raw_action = pack and 0b00000111u
					val has_line_width = pack and 0b00010000u > 0u

					when(raw_action) {
						//Line
						0u -> { actions.add(LineAction(read_point())) }
						//Horizontal Line
						1u -> { actions.add(HorizontalAction(read_unit())) }
						//Vertical Line
						2u -> { actions.add(VerticalAction(read_unit())) }

						//CubicBezier
						3u -> { actions.add(CubicBezierAction(read_point(), read_point(), read_point())) }

						//ArcCircle
						4u -> {
							val pack = read_ubyte().toUInt()
							val large_arc = pack and 0b00000001u > 0u
							val sweep = pack and 0b00000010u > 0u
							val radius = read_unit()
							val end = read_point()
							actions.add(ArcCircleAction(radius, end, large_arc, sweep))
						}

						//ArcEllipse
						5u -> {
							val pack = read_ubyte().toUInt()
							val large_arc = pack and 0b00000001u > 0u
							val sweep = pack and 0b00000010u > 0u
							val radius_x = read_unit()
							val radius_y = read_unit()
							val rotation = read_unit()
							val end = read_point()
							actions.add(ArcEllipseAction(radius_x, radius_y, end, rotation, large_arc, sweep))
						}

						//ClosePath
						6u -> { actions.add(ClosePathAction()) }

						//QuadraticBezier
						7u -> { actions.add(QuadraticBezierAction(read_point(), read_point())) }
					}
				}
			}
		}


		//
		// Commands
		//

		while (true) {
			val command_byte = read_ubyte().toUInt()
			val raw_command = command_byte and 0b00111111u
			val raw_prim_style_kind = (command_byte and 0b11000000u) shr 6

			when (raw_command) {
				//EndOfDocument
				0u -> {
					if (raw_prim_style_kind != 0u) die("Primitive Style for End Of Document was not 0")
					commands.add(EndOfDocumentCommand())

					document = Document<RawUnit, Color>(header!!, color_table, commands)

					yield(true)
					break
				}

				//FillPolygon
				1u -> {
					val point_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val points = mutableListOf<Point<RawUnit>>()
					commands.add(FillPolygonCommand(points, style))
					for (_i in 1u..point_count) {
						points.add(Point(read_unit(), read_unit()))
					}
				}

				//FillRectangles
				2u -> {
					val rectangle_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val rectangles = mutableListOf<Rectangle<RawUnit>>()
					commands.add(FillRectanglesCommand(rectangles, style))
					for (_i in 1u..rectangle_count) {
						rectangles.add(Rectangle(read_point(), read_unit(), read_unit()))
					}
				}

				//FillPath
				3u -> {
					val segment_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val segments = mutableListOf<PathSegment<RawUnit>>()
					commands.add(FillPathCommand(Path(segments), style))
					read_path(segment_count, segments)
				}

				//DrawLines
				4u -> {
					val line_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val width = read_unit()
					val lines = mutableListOf<Line<RawUnit>>()
					commands.add(DrawLinesCommand(lines, style, width))
					for (_i in 1u..line_count) {
						lines.add(Line(read_point(), read_point()))
					}
				}

				//DrawLineLoop
				5u, 6u -> {
					val point_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val width = read_unit()
					val points = mutableListOf<Point<RawUnit>>()

					when (raw_command) {
						5u -> {	commands.add(DrawLineLoopCommand(points, style, width)) }
						6u -> {	commands.add(DrawLineStripCommand(points, style, width)) }
					}

					for (_i in 1u..point_count) {
						points.add(Point(read_unit(), read_unit()))
					}
				}

				//DrawLinePath
				7u -> {
					val segment_count = read_varuint() + 1u
					val style = read_style(raw_prim_style_kind)
					val width = read_unit()
					val segments = mutableListOf<PathSegment<RawUnit>>()
					commands.add(DrawLinePathCommand(Path(segments), style, width))
					read_path(segment_count, segments)
				}

				//OutlineFillPolygon
				8u -> {
					val pack = read_ubyte().toUInt()
					val point_count = (pack and 0b00111111u) + 1u
					val raw_sec_style_kind = (pack and 0b11000000u) shr 6
					val fill_style = read_style(raw_prim_style_kind)
					val line_style = read_style(raw_sec_style_kind)
					val line_width = read_unit()
					val points = mutableListOf<Point<RawUnit>>()
					commands.add(OutlineFillPolygonCommand(points, fill_style, line_style, line_width))
					for (_i in 1u..point_count) {
						points.add(Point(read_unit(), read_unit()))						 
					}
				}

				//OutlineFillRectangles
				9u -> {
					val pack = read_ubyte().toUInt()
					val rectangle_count = (pack and 0b00111111u) + 1u
					val raw_sec_style_kind = (pack and 0b11000000u) shr 6
					val fill_style = read_style(raw_prim_style_kind)
					val line_style = read_style(raw_sec_style_kind)
					val line_width = read_unit()
					val rectangles = mutableListOf<Rectangle<RawUnit>>()
					commands.add(OutlineFillRectanglesCommand<RawUnit>(rectangles, fill_style, line_style, line_width))
					for (_i in 1u..rectangle_count) {
						rectangles.add(Rectangle(read_point(), read_unit(), read_unit()))
					}
				}

				//OutlineFillPath
				10u -> {
					val pack = read_ubyte().toUInt()
					val segment_count = (pack and 0b00111111u) + 1u
					val raw_sec_style_kind = (pack and 0b11000000u) shr 6
					val fill_style = read_style(raw_prim_style_kind)
					val line_style = read_style(raw_sec_style_kind)
					val line_width = read_unit()
					val segments = mutableListOf<PathSegment<RawUnit>>()
					commands.add(OutlineFillPathCommand(Path(segments), fill_style, line_style, line_width))
					read_path(segment_count, segments)
				}

			}
		}
	}.iterator()

	fun trailing_data(): ArrayDeque<ByteArray> {
		if (!finished) throw IllegalStateException("Attempt to access trailing data before the document has been fully parsed")
		if (!slices_cleaned_up) {
			while (slices.isNotEmpty() && i >= slices[0].size) {
				slices.removeFirstOrNull()
				i = 0
			}
			if (slices.isNotEmpty() && i > 0) {
				val old = slices[0]
				slices[0] = ByteArray(old.size - i) { val b = old[i]; i += 1; b }
			}
			slices_cleaned_up = true
		}
		return slices
	}

	override fun toString(): String {
		val builder = StringBuilder()
		val printer = FancyPrinter(
			print_func = { dat -> builder.append(dat) }
		)
		print_document_bits(
			header as HeaderData<UnitType>?,
			color_table,
			commands as List<Command<UnitType>>,
			printer
		)
		return builder.toString()
	}
}

open class ParsingException(msg: String, val engine: BinaryParsingEngine) : RuntimeException(msg) { }

class TrailingDataParsingException(engine: BinaryParsingEngine): ParsingException("There is trailing data in the TinyVG stream", engine) {}

fun binary_parse_byte_array(data: ByteArray): Document<RawUnit, Color> {
	val engine = BinaryParsingEngine()
	if (!engine.input(data)) throw ParsingException("Early end of document", engine)
	if (engine.has_trailing_data) throw TrailingDataParsingException(engine)
	return engine.document!!
}
