package dev.alador.mechanical_quill.util

import java.nio.ByteBuffer

actual class ByteCasting actual constructor () {
	private val buf = byteArrayOf(0, 0, 0, 0)
	private val view = ByteBuffer.wrap(buf)

	actual fun as_float(b1: UByte, b2: UByte, b3: UByte, b4: UByte): Float {
		buf[0] = b1.toByte()
		buf[1] = b2.toByte()
		buf[2] = b3.toByte()
		buf[3] = b4.toByte()
		return view.getFloat()
	}
}
