package dev.alador.mechanical_quill.triangulation.test.jvm

import java.io.ByteArrayOutputStream
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder

import dev.alador.mechanical_quill.P
import dev.alador.mechanical_quill.triangulation.Edge
import dev.alador.mechanical_quill.test.util.*

class EdgeCutAtTest: FunSpec({
	test("Not Colliding General") {
		Edge(P(0.0, 0.0), P(10.0, 10.0))
		.should_cut_at(Edge(P(5.0, 20.0), P(-11.0, 5.0)))
		.shouldBe(null)

		Edge(P(-1.0, -1.0), P(1.0, 1.0))
		.should_cut_at(Edge(P(-5.0, 1.0), P(-1.0, -1.0)))
		.shouldBe(null)
	}

	test("Not Colliding Horizontal") {
		Edge(P(0.0, 5.0), P(5.0, 5.0))
		.should_cut_at(Edge(P(8.0, 5.0), P(13.0, 5.0)))
		.shouldBe(null)

		Edge(P(50.0, 0.0), P(60.0, 0.0))
		.should_cut_at(Edge(P(40.0, 10.0), P(70.0, 10.0)))
		.shouldBe(null)

		Edge(P(-3.0, 3.0), P(3.0, 3.0))
		.should_cut_at(Edge(P(3.0, 3.0), P(5.0, 3.0)))
		.shouldBe(null)
	}

	test("Not Colliding Vertical") {
		Edge(P(6.0, -2.0), P(6.0, 10.0))
		.should_cut_at(Edge(P(6.0, -50.0), P(6.0, -20.0)))
		.shouldBe(null)

		Edge(P(-8.0, 10.0), P(-8.0, 0.0))
		.should_cut_at(Edge(P(-4.0, -5.0), P(-4.0, 15.0)))
		.shouldBe(null)

		Edge(P(30.0, 30.0), P(30.0, 50.0))
		.should_cut_at(Edge(P(30.0, 0.0), P(30.0, 30.0)))
		.shouldBe(null)
	}

	test("Not Colliding Vertical Horizontal") {
		Edge(P(6.0, 6.0), P(6.0, -20.0))
		.should_cut_at(Edge(P(1.0, -1.0), P(1.0, -10.0)))
		.shouldBe(null)

		Edge(P(15.0, 5.0), P(15.0, -7.5))
		.should_cut_at(Edge(P(15.0, -7.5), P(0.0, -7.5)))
		.shouldBe(null)

		Edge(P(-4.0, -5.5), P(-10.0, -5.5))
		.should_cut_at(Edge(P(100.0, 100.0), P(100.0, 225.333)))
		.shouldBe(null)
	}

	test("Basic Intersection") {
		Edge(P(0.0, 0.0), P(2.0, 2.0))
		.should_cut_at(Edge(P(0.0, 2.0), P(2.0, 0.0)))
		.shouldBe(listOf(
			P(1.0, 1.0)
		))

		Edge(P(75.0, 73.0),  P(80.0, 40.0))
		.should_cut_at(Edge(P(-10.0, -10.0), P(80.0, 40.0)))
		.shouldBe(null)

		val wack_cut = 
			Edge(P(-40.0, 50.0), P(153.6, -33.35))
			.should_cut_at(Edge(P(-30.0, -60.0), P(10.0, 1000.0)))!!

		wack_cut.size.shouldBe(1)
		wack_cut[0].x.v.should_be_tollerance(-26.075, 0.001)
		wack_cut[0].y.v.should_be_tollerance(44.005, 0.001)

		println("yes")
		Edge(P(-4.0, -2.0), P(4.0, 2.0))
		.should_cut_at(Edge(P(0.0, 0.0), P(8.0, 4.0)))
		.shouldContainExactlyInAnyOrder(
			P(0.0, 0.0),
			P(4.0, 2.0)
		)
	}

	test("Horizontal Intersection") {
		Edge(P(0.0, 0.0), P(10.0, 0.0))
		.should_cut_at(Edge(P(-10.0, 0.0), P(20.0, 0.0)))
		.shouldContainExactlyInAnyOrder(
			P(0.0, 0.0),
			P(10.0, 0.0)
		)

		Edge(P(45.0, 10.0), P(50.0, 10.0))
		.should_cut_at(Edge(P(48.333, 10.0), P(111.11, 10.0)))
		.shouldContainExactlyInAnyOrder(
			P(48.333, 10.0),
			P(50.0, 10.0),
		)
	}

	test("Vertical Intersection") {
		Edge(P(5.0, 10.0), P(5.0, -5.0))
		.should_cut_at(Edge(P(5.0, 4.5), P(5.0, -2.7)))
		.shouldContainExactlyInAnyOrder(
			P(5.0, 4.5),
			P(5.0, -2.7)
		)

		Edge(P(-75.34, -28.0), P(-75.34, -40.0))
		.should_cut_at(Edge(P(-75.34, -30.36), P(-75.34, 100.0)))
		.shouldContainExactlyInAnyOrder(
			P(-75.34, -28.0),
			P(-75.34, -30.36)
		)
	}

	test("Vertical Horizontal Intersection") {
		Edge(P(10.0, 5.0), P(10.0, 23.7))
		.should_cut_at(Edge(P(-1000.0, 17.2323), P(1000.0, 17.2323)))
		.shouldBe(listOf(P(10.0, 17.2323)))
	}

	test("Vertical General Intersection") {
		//Not colliding
		Edge(P(0.0, 10.0), P(0.0, 0.0))
		.should_cut_at(Edge(P(5.0, 5.0), P(10.0, 0.0)))
		.shouldBe(null)
	}
})

class TriangleMiscTests: FunSpec({
})
