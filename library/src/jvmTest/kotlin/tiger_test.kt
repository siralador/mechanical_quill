package dev.alador.mechanical_quill.test.jvm

import java.io.ByteArrayOutputStream
import io.kotest.core.spec.style.FunSpec

import dev.alador.mechanical_quill.parser.binary_parse_byte_array
import dev.alador.mechanical_quill.parser.ParsingException

class TigerTest: FunSpec({

	test("tiger.tvg parsing test") {
		val tiger = TigerTest::class.java.getResourceAsStream("/tiger.tvg")
		val sink = ByteArrayOutputStream()
		tiger.use { tiger.buffered().copyTo(sink) }
		try {
			println(binary_parse_byte_array(sink.toByteArray()).commands.size)
		} catch (e: ParsingException) {
			println(e.engine)
			throw e
		}
	}
	
})
