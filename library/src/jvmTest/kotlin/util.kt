package dev.alador.mechanical_quill.test.util

import kotlin.math.absoluteValue
import io.kotest.matchers.comparables.shouldBeLessThan

fun Double.should_be_tollerance(expected: Double, tollerance: Double) {
	(this - expected).absoluteValue.shouldBeLessThan(tollerance)	
}
