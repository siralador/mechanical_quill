package dev.alador.mechanical_quill.triangulation.test.jvm

import java.io.ByteArrayOutputStream
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder

import dev.alador.mechanical_quill.P
import dev.alador.mechanical_quill.triangulation.triangulate
import dev.alador.mechanical_quill.triangulation.Triangle
import dev.alador.mechanical_quill.test.util.*

class TriangulationTest: FunSpec({
	test("Triangulate A TRIANGLE!!!!!") {
		println("\nTriangulate a TRIANGLE!!!!!")
		triangulate(listOf(listOf(P(0.0, 0.0), P(3.0, 3.0), P(1.5, 3.0))))
		.shouldBe(listOf(Triangle(P(0.0, 0.0), P(3.0, 3.0), P(1.5, 3.0))))

		//Same triangle, but it connects back to the origin
		triangulate(listOf(listOf(P(0.0, 0.0), P(3.0, 3.0), P(1.5, 3.0), P(0.0, 0.0))))
		.shouldBe(listOf(Triangle(P(0.0, 0.0), P(3.0, 3.0), P(1.5, 3.0))))
	}

	test("Triangulate a SQUARE!!!!") {
		println("\nTriangulate a SQUARE!!!!!")
		val list = triangulate(listOf(listOf(
			P(0.0, 0.0),
			P(1.0, 0.0),
			P(1.0, 1.0),
			P(0.0, 1.0),
			P(0.0, 0.0)
		)))
		list.size.shouldBe(2)
		val points = setOf(
			*list[0].points(),
			*list[1].points()
		)
		points.shouldContainExactlyInAnyOrder(
			P(0.0, 0.0),
			P(1.0, 0.0),
			P(1.0, 1.0),
			P(0.0, 1.0)
		)
	}
})
