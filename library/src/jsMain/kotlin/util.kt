package dev.alador.mechanical_quill.util

external private class Uint8Array(array: Array<UByte>) {
	val buffer: ArrayBuffer
	operator fun get(index: Int): Any
	operator fun set(index: Int, value: Any)
}
external private class ArrayBuffer {
	val byteLength: Int
}
external private class DataView(buffer: ArrayBuffer, byte_offset: Int, byte_length: Int) {
	fun getFloat32(byte_offset: Int, littleEndian: Boolean): Float
}

actual class ByteCasting actual constructor() {
	private val buf = Uint8Array(arrayOf(0u, 0u, 0u, 0u))
	private val view = DataView(buf.buffer, 0, buf.buffer.byteLength)

	actual fun as_float(b1: UByte, b2: UByte, b3: UByte, b4: UByte): Float {
		buf[0] = b1
		buf[1] = b2
		buf[2] = b3
		buf[3] = b4
		return view.getFloat32(0, false)
	}
}
