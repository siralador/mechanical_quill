repositories {
	mavenCentral()
}

plugins {
	java
	application
	id("org.openjfx.javafxplugin") version "0.0.13"
}

javafx {
	version = "18"
	modules = listOf( "javafx.controls" )
}
